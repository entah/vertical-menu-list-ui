/**

   VerticalMenuListUI.h

   Vertical Menu List UI to used with u8g2lib.h

   Copyright 2023 Ragil Rynaldo Meyer <meyer.ananda@gmail.com>

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.

*/

#ifndef VERTICAL_MENU_LIST_UI
#define VERTICAL_MENU_LIST_UI

#pragma once

#include <Arduino.h>
#include <U8g2lib.h>
#include <functional>
#include <string.h>

#ifndef VMENU_LIST_UI_MAX_MENU_ITEM_STORED
#define VMENU_LIST_UI_MAX_MENU_ITEM_STORED 8
#endif
#ifndef VMENU_LIST_UI_MAX_MENU_ITEM_LABEL_LENGTH
#define VMENU_LIST_UI_MAX_MENU_ITEM_LABEL_LENGTH 63
#endif
#ifndef VMENU_LIST_UI_REFRESH_UPDATE_LONG_LABEL
#define VMENU_LIST_UI_REFRESH_UPDATE_LONG_LABEL 800
#endif
#ifndef VMENU_LIST_UI_FONT_ALPHABET_FONT
#define VMENU_LIST_UI_FONT_ALPHABET_FONT u8g2_font_profont12_mr
#endif

/**
typedef vmenuItemCb_t

Function to call when execute each item.

@return void
*/
typedef std::function<void()> vmenuItemCb_t;

/**
class VMenuItemCursorCallback

Use to fetch item more than `VMENU_LIST_UI_MAX_MENU_ITEM_STORED`.

*/
class VMenuItemCursorCallback {
public:
  explicit VMenuItemCursorCallback();
  virtual bool nextItem(uint8_t cIndex);
  virtual bool prevItem(uint8_t cIndex);
  virtual char *label();
  virtual void handler(vmenuItemCb_t *);
}; // class VMenuItemCursorCallback

/**
class VerticalMenuListUI

Used to render Vertical Menu List UI.

@param `*display` - pointer to display used.
@param `startYPos` - Y position to start render the UI, default zero to show
full display height.
*/
class VerticalMenuListUI {
public:
  explicit VerticalMenuListUI(U8G2 *display, uint8_t startYPos);

  // Use in loop to update the display.
  void loop();

  // Scroll next a long message or next item to show.
  void next();

  // Scroll back a long message or prev item to show.
  void prev();

  // Show the UI.
  void show();

  // Hide the UI.
  void hide();

  // Reset all values but show/hidden value.
  void reset();

  // Label of current active/highlighted item.
  char *currentActiveMenuLabel();

  // Execute active/highlighted item if available.
  void executeActiveMenuHandler();

  // Return index of active/highlighted item.
  uint8_t currentActiveMenuIndex();

  // Set Y position to start render the UI
  void setYPos(uint8_t startYPos);

  // Set total menu item, if the item is greater than
  // `VMENU_LIST_UI_MAX_MENU_ITEM_STORED`.
  void setTotalMenuItem(uint8_t total);

  // Set callback for next/prev item.
  void setItemCursorCallback(VMenuItemCursorCallback *cb);

  // Return true if stored menu list item has not full yet.
  bool appendMenu(const char *label, vmenuItemCb_t handler);

private:
  // variable to hold a list of menu item handler.
  vmenuItemCb_t mMenuItemHandlers[VMENU_LIST_UI_MAX_MENU_ITEM_STORED];
  // variable to hold a list of menu item label.
  char *pMenuItemLabels[VMENU_LIST_UI_MAX_MENU_ITEM_STORED];
  // variable to indicate hidden menu item.
  uint8_t mPrevMenuItem = 0;
  // variable to indicate total menu item.
  uint8_t mTotalMenuItem = 0;
  // variable to indicate UI should be hidden.
  bool mHidden = true;
  // variable to indicate index of active/highlighted menu item.
  uint8_t mActiveMenuItemIndex = 0;
  // variable to indecate how many menu item appended.
  uint8_t mFilledItems = 0;
  // variable to hold handler for requesting a new list of item.
  vmenuICurCb_t mReqItemHandler = NULL;
  // variable to indicate Y position of display to render.
  uint8_t mStartYPos = 0;
  // variable hold the pointer of display object.
  U8G2 *pDisplay = NULL;
  // variable to indicate last time active/highlighted item has been rendered.
  long mLastUpdateLongLabel = 0;
  // variable to indicate index of first item shown in display.
  uint8_t mShownLabelFirstIndex = 0;
  // variable to indicate how many char can be shown relative to display width.
  uint8_t mLableShownCharLen = 0;
  // do update for long label menu item.
  void updateLongLabelVar();
  // callback for fetch next or previous item.
  VMenuItemCursorCallback *pItemCursorCb = NULL;
}; // class VerticalMenuListUI

#endif

// Local Variables:
// mode: c++
// End:
