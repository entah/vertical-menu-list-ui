/**

   VerticalMenuListUI.cpp implementation of VerticalMenuListUI.h

   Vertical Menu List UI to used with u8g2lib.h

   Copyright 2023 Ragil Rynaldo Meyer <meyer.ananda@gmail.com>

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.

*/

#include "VerticalMenuListUI.h"

VMenuItemCursorCallback::VMenuItemCursorCallback() {}

bool VMenuItemCursorCallback::nextItem(uint8_t cIndex) { return false; }

bool VMenuItemCursorCallback::prevItem(uint8_t cIndex) { return false; }

char *VMenuItemCursorCallback::label() { return NULL; }
void VMenuItemCursorCallback::handler(vmenuItemCb_t *) {}

VerticalMenuListUI::VerticalMenuListUI(U8G2 *display, uint8_t startYPos = 0) {
  this->pDisplay = display;
  this->mStartYPos = startYPos;
  for (uint8_t i = 0; i < VMENU_LIST_UI_MAX_MENU_ITEM_STORED; i++) {
    char *init = new char[VMENU_LIST_UI_MAX_MENU_ITEM_LABEL_LENGTH];
    memset(init, 0, VMENU_LIST_UI_MAX_MENU_ITEM_LABEL_LENGTH);
    this->pMenuItemLabels[i] = init;
  }

  display->setFont(VMENU_LIST_UI_FONT_ALPHABET_FONT);
  uint8_t maxCharWidth = display->getMaxCharWidth();
  uint8_t displayWidth = display->getDisplayWidth();

  this->mLableShownCharLen = (displayWidth / maxCharWidth) - 1;
} // VerticalMenuListUI::VerticalMenuListUI

void VerticalMenuListUI::updateLongLabelVar() {
  if (strlen(this->currentActiveMenuLabel()) > this->mLableShownCharLen) {
    this->mLastUpdateLongLabel = 0;
    this->mShownLabelFirstIndex = 0;
  }
} // void VerticalMenuListUI::updateLongLabelVar

bool VerticalMenuListUI::appendMenu(const char *label, vmenuItemCb_t handler) {
  if (this->mFilledItems == VMENU_LIST_UI_MAX_MENU_ITEM_STORED)
    return false;

  this->mMenuItemHandlers[this->mFilledItems] = handler;
  strncpy(this->pMenuItemLabels[this->mFilledItems], label,
          VMENU_LIST_UI_MAX_MENU_ITEM_LABEL_LENGTH);
  this->mFilledItems += 1;
  return true;
} // bool VerticalMenuListUI::appendMenu

void VerticalMenuListUI::setTotalMenuItem(uint8_t total) {
  this->mTotalMenuItem = total;
} // void VerticalMenuListUI::setTotalMenuItem

void VerticalMenuListUI::setOnReqItemHandler(vmenuICurCb_t handler) {
  this->mReqItemHandler = handler;
} // void VerticalMenuListUI::setOnReqItemHandler

void VerticalMenuListUI::setItemCursorCallback(VMenuItemCursorCallback *cb) {
  this->pItemCursorCb = cb;
} // void VerticalMenuListUI::setItemCursorCallback

void VerticalMenuListUI::setYPos(uint8_t startYPos) {
  this->mStartYPos = startYPos;
} // void VerticalMenuListUI::setYPos

void VerticalMenuListUI::show() { this->mHidden = false; };

void VerticalMenuListUI::hide() { this->mHidden = true; };

void VerticalMenuListUI::next() {
  if (this->mHidden)
    return;

  uint8_t total =
      this->mTotalMenuItem ? this->mTotalMenuItem : this->mFilledItems;

  // predict if we need fetch a new set of menu item.
  bool outOfTotalStored = total > VMENU_LIST_UI_MAX_MENU_ITEM_STORED;
  uint8_t totalKnownMenu = this->mPrevMenuItem + this->mFilledItems;

  if (this->mActiveMenuItemIndex == this->mFilledItems - 1 &&
      totalKnownMenu < total && outOfTotalStored) {
    // return if no handler registered
    if (!this->pItemCursorCb)
      return;

    bool success = this->pItemCursorCb->nextItem(this->mActiveMenuItemIndex +
                                                 this->mPrevMenuItem);

    if (success) {
      uint8_t lIndex = this->mFilledItems - 1;
      for (uint8_t i = 0; i < lIndex; i++) {
        this->mMenuItemHandlers[i] = this->mMenuItemHandlers[i + 1];
        memset(pMenuItemLabels[i], 0, VMENU_LIST_UI_MAX_MENU_ITEM_LABEL_LENGTH);
        strcpy(pMenuItemLabels[i], pMenuItemLabels[i + 1]);
      }

      this->pItemCursorCb->handler(&this->mMenuItemHandlers[lIndex]);
      memset(pMenuItemLabels[lIndex], 0,
             VMENU_LIST_UI_MAX_MENU_ITEM_LABEL_LENGTH);
      strcpy(pMenuItemLabels[lIndex], this->pItemCursorCb->label());
      this->mPrevMenuItem += 1;
      updateLongLabelVar();
    }
    return;
  }

  if (this->mActiveMenuItemIndex < this->mFilledItems - 1)
    this->mActiveMenuItemIndex += 1;

  this->updateLongLabelVar();
} // void VerticalMenuListUI::next

void VerticalMenuListUI::prev() {
  if (this->mHidden)
    return;

  uint8_t total =
      this->mTotalMenuItem ? this->mTotalMenuItem : this->mFilledItems;

  bool outOfTotalStored = total > VMENU_LIST_UI_MAX_MENU_ITEM_STORED;

  if (this->mActiveMenuItemIndex == 0 && this->mPrevMenuItem > 0 &&
      outOfTotalStored) {

    if (!this->pItemCursorCb)
      return;

    bool success = this->pItemCursorCb->prevItem(this->mPrevMenuItem);

    if (success) {
      for (uint8_t i = this->mFilledItems - 1; i > 0; i--) {
        this->mMenuItemHandlers[i] = this->mMenuItemHandlers[i - 1];
        memset(pMenuItemLabels[i], 0, VMENU_LIST_UI_MAX_MENU_ITEM_LABEL_LENGTH);
        strcpy(pMenuItemLabels[i], pMenuItemLabels[i - 1]);
      }
      this->pItemCursorCb->handler(&this->mMenuItemHandlers[0]);
      memset(pMenuItemLabels[0], 0, VMENU_LIST_UI_MAX_MENU_ITEM_LABEL_LENGTH);
      strcpy(pMenuItemLabels[0], this->pItemCursorCb->label());
      this->mPrevMenuItem -= 1;
    }
  }

  if (this->mActiveMenuItemIndex > 0)
    this->mActiveMenuItemIndex -= 1;

  this->updateLongLabelVar();
} // void VerticalMenuListUI::prev

void VerticalMenuListUI::executeActiveMenuHandler() {
  if (this->mHidden)
    return;

  vmenuItemCb_t activeHandler =
      this->mMenuItemHandlers[this->mActiveMenuItemIndex];
  if (activeHandler)
    activeHandler();
} // void VerticalMenuListUI::executeActiveMenuHandler

uint8_t VerticalMenuListUI::currentActiveMenuIndex() {
  return this->mActiveMenuItemIndex;
} // uint8_t VerticalMenuListUI::currentActiveMenuIndex

char *VerticalMenuListUI::currentActiveMenuLabel() {
  return this->pMenuItemLabels[this->mActiveMenuItemIndex];
} // char *VerticalMenuListUI::currentActiveMenuLabel

void VerticalMenuListUI::reset() {
  for (uint8_t i = 0; i < VMENU_LIST_UI_MAX_MENU_ITEM_STORED; i++) {
    this->mMenuItemHandlers[i] = 0;
    memset(this->pMenuItemLabels[i], 0,
           VMENU_LIST_UI_MAX_MENU_ITEM_LABEL_LENGTH);
  }
  this->mActiveMenuItemIndex = 0;
  this->mFilledItems = 0;
  this->mPrevMenuItem = 0;
  this->mTotalMenuItem = 0;
  this->mReqItemHandler = 0;
} // void VerticalMenuListUI::reset

void VerticalMenuListUI::loop() {
  if (this->mHidden)
    return;

  this->pDisplay->setDrawColor(1);
  this->pDisplay->setFont(
      VMENU_LIST_UI_FONT_ALPHABET_FONT); // width = 6, height = 12

  uint8_t maxCharHeight = this->pDisplay->getMaxCharHeight();
  uint8_t yPos = this->mStartYPos + 1 + maxCharHeight;
  uint8_t maxMenuItemShowing =
      (this->pDisplay->getDisplayHeight() - this->mStartYPos) / maxCharHeight;

  bool timeUpdateReached = millis() - this->mLastUpdateLongLabel >
                           VMENU_LIST_UI_REFRESH_UPDATE_LONG_LABEL;

  for (uint8_t i = 0; i < maxMenuItemShowing; i++) {
    uint8_t index =
        this->mActiveMenuItemIndex < maxMenuItemShowing
            ? i
            : i + (this->mActiveMenuItemIndex - maxMenuItemShowing) + 1;

    uint8_t isActive = index == this->mActiveMenuItemIndex;

    if (isActive) {
      this->pDisplay->drawFrame(
          0, yPos - maxCharHeight + 1 + ((maxCharHeight + 2) * i),
          this->pDisplay->getDisplayWidth() - 2, maxCharHeight + 2);
    }

    char *label = this->pMenuItemLabels[index];
    uint8_t labelLen = strlen(label);
    bool longLabel = labelLen > this->mLableShownCharLen;

    this->pDisplay->setCursor(3, yPos + ((maxCharHeight + 2) * i));

    if (longLabel) {
      bool shownAllChar =
          (this->mShownLabelFirstIndex + this->mLableShownCharLen) == labelLen;
      if (isActive) {
        for (uint8_t j = 0; j < this->mLableShownCharLen; j++) {
          uint8_t charIndex = j + this->mShownLabelFirstIndex;
          this->pDisplay->print(label[charIndex]);
        }

        if (timeUpdateReached) {
          this->mLastUpdateLongLabel = millis();
          this->mShownLabelFirstIndex =
              !shownAllChar ? this->mShownLabelFirstIndex += 1 : 0;
        }
        continue;
      }

      for (uint8_t j = 0; j < this->mLableShownCharLen - 3; j++)
        this->pDisplay->print(label[j]);
      this->pDisplay->print("...");
      continue;
    }
    this->pDisplay->print(label);
  }
} // void VerticalMenuListUI::loop
